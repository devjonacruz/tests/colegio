<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Callback payments
 */
Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'payments'], function () {
        Route::post('coinpayments/confirmation', "\App\Services\Payments\Coinpayments@confirmation");
    });
});

Route::group(['middleware' => ['auth']], function () {
    /**
     * Backend
     */
    Route::group(['prefix' => 'api'], function () {
        Route::prefix('admin')->middleware(['role:Admin'])->group(function () {
            Route::apiResource('users', Admin\UserController::class)->names([
                'store' => 'api.admin.users.store',
                'update' => 'api.admin.users.update',
                'destroy' => 'api.admin.users.destroy',
            ])->only(['store', 'update', 'destroy']);

            Route::put('orderPackage/approved/{ordersPackage}', "Admin\OrdersPackageController@approved")->name('api.orderPackage.approved');
            Route::put('orderPackage/cancelled/{ordersPackage}', "Admin\OrdersPackageController@cancelled")->name('api.orderPackage.cancelled');
            Route::put('transactions/approved/{transaction}', "Admin\TransactionController@approved")->name('api.transactions.approved');
            Route::put('transactions/cancelled/{transaction}', "Admin\TransactionController@cancelled")->name('api.transactions.cancelled');

            Route::apiResource('settings', Admin\SettingController::class)->names([
                'store' => 'api.admin.settings.store',
                'update' => 'api.admin.settings.update',
                'destroy' => 'api.admin.settings.destroy',
            ])->only(['store', 'update', 'destroy']);
        });

        Route::apiResource('users', UserController::class)->names([
            'update' => 'api.users.update',
        ])->only([
            // 'index',
            'update',
        ]);

        Route::put('userAuthentication/{user}', "UserAuthenticationController@update")->name('api.usersauthetication.update');
        Route::put('userWallet/{user}', "UserWalletController@update")->name('api.userswallet.update');
        Route::post('userWallet/{user}', "UserWalletController@store")->name('api.userswallet.store');
        Route::put('userBroker/{user}', "UserBrokerController@update")->name('api.usersBroker.update');
        Route::post('orderPackage/{user}/{package}', "OrdersPackageController@store")->name('api.orderPackage.store');
        Route::post('tickets/{user}', "StatesTicketController@store")->name('api.tickets.store');
        Route::put('tickets/{user}/{supportTicket}', "StatesTicketController@update")->name('api.tickets.update');
    });




    /**
     * Frontend
     */
    Route::prefix('admin')->middleware(['role:Admin'])->group(function () {
        Route::resource('users', Admin\UserController::class)->names([
            'index' => 'admin.users.index',
            'edit' => 'admin.users.edit',
        ])->only(['index', 'create', 'show', 'edit']);
        Route::get('orders', 'Admin\OrdersPackageController@index')->name('admin.ordersPackages.index');
        Route::get('transactions', 'Admin\TransactionController@index')->name('admin.transactions.index');
        Route::get('settings', 'Admin\SettingController@index')->name('admin.settings.index');
        Route::get('settings/create', 'Admin\SettingController@create')->name('admin.settings.create');
        Route::get('settings/{setting}', 'Admin\SettingController@edit')->name('admin.settings.edit');
        Route::prefix('history')->group(function () {
            Route::get('supports', 'Admin\HistoryController@supports')->name('admin.histories.supports');
            Route::get('commissions', 'Admin\HistoryController@commissions')->name('admin.histories.commissions');
            Route::get('users', 'Admin\HistoryController@users')->name('admin.histories.users_lost');
        });
    });

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => [env('IS_EMAIL_VERIFIED') ? 'verified' : 'auth']], function () {
        Route::resource('users', UserController::class)->only(['edit']);
        Route::get('users/tree', 'UserController@tree')->name('users.tree');
        Route::resource('users.referrals', UserReferralController::class)->only(['index']);
        Route::resource('users.wallets', UserWalletController::class)->only(['index']);
        Route::resource('packages', PackagesController::class)->only(['index']);
        Route::get('orders', 'OrdersPackageController@index')->name('ordersPackages.index');
        Route::resource('commissions', CommissionController::class)->only(['index']);
        Route::resource('supportTickets', StatesTicketController::class)->only(['index', 'create', 'show']);
    });
});

Route::get('register/{parent?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Auth::routes(['verify' => true]);

// // Authentication Routes...
// Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('login', 'Auth\LoginController@login');
// Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// // Registration Routes...
// Route::get('register/{parent?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// // Password Reset Routes...
// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');
