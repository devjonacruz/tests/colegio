@extends('layouts.app')

@section('content-header')
<section class="content-header">
    <h1>
        Ticket
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Ticket</li>
    </ol>
</section>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="box direct-chat">
            <div class="box-header with-border">
                <h3 class="box-title">Ticket History</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->
                <div id="chat-app" class="direct-chat-messages chat-app">
                    @foreach($supportTicket->comments as $comment)

                    @if(Auth::user()->id == $comment->user->id)
                    <div class="direct-chat-msg right mb-30">
                        <div class="clearfix mb-15">
                            <span class="direct-chat-name pull-right">{{ $comment->user->name }}</span>
                            <span class="direct-chat-timestamp ">{{ $comment->created_at->diffForHumans() }}</span>
                        </div>
                        <!-- /.direct-chat-info -->
                        <img class="direct-chat-img avatar" src="/template/images/user3-128x128.jpg"
                            alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            {{ $comment->comment }}
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>

                    @else
                    <div class="direct-chat-msg mb-30">
                        <div class="clearfix mb-15">
                            <span class="direct-chat-name">{{ $comment->user->name }}</span>
                            <span class="direct-chat-timestamp pull-right">
                                {{ $comment->created_at->diffForHumans() }}
                            </span>
                        </div>
                        <!-- /.direct-chat-info -->
                        <img class="direct-chat-img avatar" src="/template/images/user1-128x128.jpg"
                            alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            {{ $comment->comment }}
                        </div>

                        <!-- /.direct-chat-text -->
                    </div>
                    @endif
                    @endforeach
                </div>
                <!--/.direct-chat-messages-->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <form-component method="PUT"
                    action="{{ route('api.tickets.update', [Auth::user()->username,$supportTicket->id]) }}"
                    v-slot="slotProps">
                    <div class="input-group">
                        <input type="text" name="comment" id="comment" placeholder="Type Message ..."
                            class="form-control" required>
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning" @click="slotProps.onSubmit">Send</button>
                        </span>
                    </div>
                </form-component>
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /. box -->
    </div>
    <!-- /.col -->
</div>

@endsection

@section('js')
<script>

</script>
@endsection