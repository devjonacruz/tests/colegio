@extends('layouts.app')

@section('content-header')
<section class="content-header">
    <h1>
        Tickets
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Tickets</li>
    </ol>
</section>
@endsection

@section('content')

@if(isset($total))
<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="box box-inverse box-info pull-up bg-hexagons-dark">
            <div class="box-body text-center">
                <div class="flexbox align-content-end">
                    <h2 class="text-white mb-10">Total Tickets</h2>
                    <h1 class="font-light text-white">{{ $total }}</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="box box-success box-inverse pull-up bg-hexagons-dark">
            <div class="box-body text-center">
                <div class="flexbox align-content-end">
                    <h2 class="text-white mb-10">Completado</h2>
                    <h1 class="font-light text-white">{{ $responded }}</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="box box-inverse box-dark pull-up bg-hexagons-dark">
            <div class="box-body text-center">
                <div class="flexbox align-content-end">
                    <h2 class="text-white mb-10">Pendiente</h2>
                    <h1 class="font-light text-white">{{ $resolve }}</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="box box-inverse box-warning pull-up bg-hexagons-white">
            <div class="box-body text-center">
                <div class="flexbox align-content-end">
                    <h2 class="text-white mb-10">Nuevo</h2>
                    <h1 class="font-light text-white">{{ $pending }}</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@endif

<div class="d-flex justify-content-end align-items-center p-2">
    <a href="{{ route('supportTickets.create') }}" class="btn btn-info text-white">
        Crear Soporte
    </a>
</div>

<div class="row">
    <div class="col-12">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="tickets" class="table mt-0 no-wrap {{ isset($datatable) ? 'data-table' : '' }} {{ isset($export) ? 'export-table' : '' }}" data-page-size="10">
                        <thead>
                            <tr class="bg-light">
                                <th>ID #</th>
                                <th>Opened By</th>
                                <th>Cust. Email</th>
                                <th>Sbuject</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td>{{ $ticket->id }}</td>
                                <td>{{ $ticket->user->username }}</td>
                                <td>{{ $ticket->user->email }}</td>
                                <td>{{ $ticket->subject }}</td>
                                <td><span
                                        class="label label-{{ $ticket->states_ticket_id == 1 ? 'warning': ($ticket->states_ticket_id == 2 ? 'inverse':'success')}}">{{ $ticket->state->name }}</span>
                                </td>
                                <td>{{ $ticket->created_at->diffForHumans() }}</td>
                                <td>
                                    @role('Admin')
                                    {{-- <button type="button" class="btn btn-sm btn-danger-outline" data-toggle="tooltip"
                                        data-original-title="Delete">
                                        <i class="ti-trash" aria-hidden="true"></i>
                                    </button> --}}
                                    @endrole
                                    <a href="{{ route('supportTickets.show', $ticket->id) }}"
                                        class="btn btn-sm btn-success-outline">
                                        <i class="ti-eye" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>

</script>
@endsection