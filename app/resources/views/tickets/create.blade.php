@extends('layouts.app')

@section('content')


<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Información Financiera</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col">
                <form-component method="POST" action="{{ route('api.tickets.store', Auth::user()->username) }}"
                    v-slot="slotProps">

                    <div class="form-group">
                        <h5>Asunto <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="subject" id="subject" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Comentario <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="comment" id="comment" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="d-flex justify-content-end">
                        <p>
                            <a class="btn btn-info" href="{{ route('supportTickets.index') }}">Cancelar</a>
                            <input class="btn btn-success" type="button" name="enviar" value="Guardar cambios"
                                @click="slotProps.onSubmit" />
                        </p>
                    </div>
                </form-component>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
@endsection