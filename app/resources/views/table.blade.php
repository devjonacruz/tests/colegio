@extends('layouts.app')

@if(isset($title))
@section('content-header')
<section class="content-header">
    <h1>
        {{ $title }}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">{{ $title }}</li>
    </ol>
</section>
@endsection
@endif


@section('content')
<div class="box">
    <div class="box-body">
        @include('layouts.default.table')
    </div>
</div>
@endsection

@section('js')
@endsection