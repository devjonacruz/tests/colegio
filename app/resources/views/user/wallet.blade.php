@extends('layouts.app')

@section('content')

<div class="row">
    @foreach(Auth::user()->wallets as $wallet)
    @php
    if(!$wallet->type_wallet->is_available) continue;
    @endphp
    <div class="col-lg-3 col-12">
        <div class="box box-body pull-up bg-hexagons-white">
            <div class="media align-items-center p-0">
                <div class="text-center">
                    <a href="#"><i class="cc DASH mr-5" title="DASH"></i></a>
                </div>
                <div>
                    <h2 class="no-margin">{{ $wallet->type_wallet->name }}</h2>
                </div>
            </div>
            <div class="flexbox align-items-center mt-20">
                <div>
                    <p class="no-margin">
                        <span class="text-info">{{ $wallet->amount }}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Realizar retiro</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            @foreach(Auth::user()->wallets as $wallet)
            @php
            if(!$wallet->type_wallet->is_available) continue;
            @endphp
            <div class="col">
                <form-component method="POST" action="{{ route('api.userswallet.store', $user->username) }}"
                    v-slot="slotProps">

                    <input type="hidden" name="type" id="type" value="{{ $wallet->type_wallet_id }}" />
                    <div class="form-group">
                        <h5>Amount <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="amount" id="amount" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Code 2FA <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="code_2fa" id="code_2fa" class="form-control" value="" required>
                        </div>
                    </div>

                    <div class="text-xs-right">
                        <button type="button" class="btn btn-info" @click="slotProps.onSubmit">Realizar retiro</button>
                    </div>
                </form-component>
            </div>
            @endforeach
        </div>
    </div>

</div>
<div class="box">
    <div class="box-body">
        @include('layouts.default.table')
    </div>
</div>

@endsection

@section('js')
<script>

</script>
@endsection