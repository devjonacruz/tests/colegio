@extends('layouts.app')

@section('content-header')
<section class="content-header">
    <h1>
        Editar usuario
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Forms</a></li>
        <li class="breadcrumb-item active">Editar usuario</li>
    </ol>
</section>
@endsection

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Información personal</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <div class="row">
            <div class="col">
                <form-component method="PUT" action="{{ route('api.admin.users.update', $user->username) }}"
                    v-slot="slotProps">
                    <div class="form-group">
                        <h5>Nombres <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}"
                                required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Apellidos <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="last_name" id="last_name" class="form-control"
                                value="{{ $user->last_name }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Usuario <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="username" id="username" class="form-control"
                                value="{{ $user->username }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Email <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}"
                                required>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Phone</h5>
                        <div class="controls">
                            <input type="text" name="phone" id="phone" class="form-control" value="{{ $user->phone }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Password</h5>
                        <div class="controls">
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Password Confirmation</h5>
                        <div class="controls">
                            <input type="password" name="password_confirmation" id="password_confirmation"
                                class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" id="code_2fa" name="code_2fa" class="filled-in"
                            {{ $user->authentication && $user->authentication->is_google ? 'checked':'' }} />
                        <label for="code_2fa">2FA</label>
                    </div>

                    <div class="text-xs-right">
                        <button type="button" class="btn btn-info" @click="slotProps.onSubmit">Actualizar información</button>
                    </div>
                </form-component>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>

</script>
@endsection