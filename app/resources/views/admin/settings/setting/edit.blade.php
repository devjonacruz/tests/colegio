@extends('layouts.app')

@section('content-header')
<section class="content-header">
    <h1>
        Editar configuración {{ $setting->settins_type->name }}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Editar</li>
    </ol>
</section>
@endsection

@section('content')


@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Información personal</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

        <div class="row">
            <div class="col">
                <form-component method="PUT" action="{{ route('api.admin.settings.update', $setting->alias) }}"
                    v-slot="slotProps">
                    <div class="form-group">
                        <label for="name">Valor</label>
                        <input class="form-control" type="text" name="value" id="value" value="{{ $setting->log }}"
                            autofocus />
                    </div>


                    <div class="d-flex justify-content-end">
                        <p>
                            <a class="btn btn-info" href="{{ route('admin.settings.index') }}">Cancelar</a>
                            <input class="btn btn-success" @click="slotProps.onSubmit" value="Guardar cambios" />
                        </p>
                    </div>
                </form-component>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
@endsection