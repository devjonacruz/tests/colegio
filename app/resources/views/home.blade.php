@extends('layouts.app')

@section('content')



<div class="tradingview-widget-container__widget"></div>
<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" defer>
    {
        "symbols": [
            {
                "proName": "FX_IDC:EURUSD",
                "title": "EUR/USD"
            },
            {
                "description": "USD/JPY",
                "proName": "KRAKEN:USDJPY"
            },
            {
                "description": "EUR/JPY",
                "proName": "KRAKEN:EURJPY"
            },
            {
                "description": "GBP/USD",
                "proName": "KRAKEN:GBPUSD"
            },
            {
                "description": "AUD/USD",
                "proName": "KRAKEN:AUDUSD"
            },
            {
                "description": "USD/CHF",
                "proName": "KRAKEN:USDCHF"
            }
        ],
        "colorTheme": "dark",
        "isTransparent": false,
        "showSymbolLogo": true,
        "displayMode": "compact",
        "locale": "es",
        "container_id": "tradingview_d5e85"
    }
</script>

<div class="row">
    <div class="col-12">
        <div class="box box-dark">
            <div class="box-body d-none">
                <ul id="webticker-1 d-none">
                    <li class="br-1">
                        <div class="mx-20">
                            <div class="d-flex justify-content-between">
                                <h5 class="mr-40 text-white-50">BCH/BTC</h5>
                                <p class="text-danger">-2.24%</p>
                            </div>
                            <div class="d-flex justify-content-between">
                                <h4 class="my-0 mr-40">$0.04886</h4>
                                <span class="sparklines">8,4,0,0,1,4,4,10,10,0,0,4,6,5,9,10</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xl-8 col-12">
        <div id="tradingview_d5e85"></div>
        <div class="box d-none">
            <div class="box-header with-border">
                <h4 class="box-title">Price Chart</h4>
            </div>
            <div class="box-body">
                <div class="chart">
                    <div id="chartdivnew" style="height: 630px"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-12">
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Disponible para retirar</h4>
            </div>
            <div class="box-body">
                <div class="d-flex justify-content-between bb-1 pb-5">
                    <h5 class="text-fade">$</h5>
                    <h5 class="text-fade">Monto</h5>
                </div>
                @foreach(Auth::user()->wallets as $wallet)
                @php
                if(!$wallet->type_wallet->is_available) continue;
                @endphp
                <div class="d-flex justify-content-between pb-5 pt-10">
                    <h4 class="text-white">{{ $wallet->type_wallet->name }}</h4>
                    <h4 class="text-white">{{ $wallet->amount }}</h4>
                </div>
                @endforeach
                <div class="d-flex justify-content-between">
                    <button type="button" class="btn btn-danger-outline btn-lg d-none">
                        <i class="fa fa-arrow-circle-down"></i> DEPOSIT
                    </button>
                    <a href="{{ route('users.wallets.index', Auth::user()->username) }}"
                        class="btn btn-success-outline btn-lg mt-0">
                        <i class="fa fa-arrow-circle-up"></i> WITHDRAW
                    </a>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body tab-content">
                <h6 class="text-white text-left mb-2">
                    Renovación <span class="text-bold">
                        {{ Auth::user()->active_end > now() ? \Carbon\Carbon::parse(Auth::user()->active_end)->diffForHumans() : '0 días' }}</span>
                </h6>
                {{-- <div class="progress mb-2">
                    <div class="
                        progress-bar
                        progress-bar-warning
                        progress-bar-striped
                        progress-bar-animated
                      " role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                        <span class="text-left pl-2">60%</span>
                    </div>
                </div> --}}
                <div class="chart">
                    <canvas id="chart_6" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 d-none" style="margin-bottom: 1rem !important;">
        {{-- <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Trading Platform</h4>
            </div>
            <div class="box-body"> --}}
        {{-- <div id="tradingview_d5e85"></div> --}}
        {{-- </div>
        </div> --}}
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
<script type="text/javascript">
    window.onload = function(){
        let ctx6 = document.getElementById("chart_6").getContext("2d");
        
        let data6 = {
          labels: ['Activo', 'Pendiente'],
          datasets: [
            {
              data: [{{ $diffInDaysActive }}, {{ $diffInDaysPending }}],
              backgroundColor: [
                "#fbae1c",
                '#fcf0e3',
              ],
              hoverBackgroundColor: [
                "#fcf0e3",
                '#fcf0e3',
              ],
            },
          ],
        };

        new Chart(ctx6, {
              type: "pie",
              data: data6,
              options: {
                animation: {
                  duration: 3000,
                },
                responsive: true,
                legend: {
                  labels: {
                    fontFamily: "Poppins",
                    fontColor: "#878787",
                  },
                },
                tooltip: {
                  backgroundColor: "rgba(33,33,33,1)",
                  cornerRadius: 0,
                  footerFontFamily: "'Poppins'",
                },
                elements: {
                  arc: {
                    borderWidth: 0,
                  },
                },
              },
            });
            
        new TradingView.widget(
            {
            // "width": 980,
            "width": "100%",
            "height": 610,
            "symbol": "NASDAQ:AAPL",
            "interval": "D",
            "timezone": "Etc/UTC",
            "theme": "dark",
            "style": "1",
            "locale": "es",
            "toolbar_bg": "#f1f3f6",
            "enable_publishing": false,
            "allow_symbol_change": true,
            "container_id": "tradingview_d5e85"
        }
        );
    }
</script>

@endsection