<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/png" sizes="32x32" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Template -->

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="/template/html/css/bootstrap-extend.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="/template/html/css/master_style.css" />

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="/template/html/css/skins/_all-skins.css" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini fixed">
    <div class="wrapper" id="app">

        @include('layouts.ui.modal')

        @include('layouts.partials.header')

        @include('layouts.partials.sidebar_left')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            @yield('content-header')

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>

        @include('layouts.partials.main_footer')

        @include('layouts.partials.sidebar_right')

        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>

    <!-- Slimscroll -->
    <script src="/template/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js" defer></script>

    <!-- FastClick -->
    <script src="/template/assets/vendor_components/fastclick/lib/fastclick.js" defer></script>

    <!-- Resources -->
    <script src="https://www.amcharts.com/lib/4/core.js" defer></script>
    <script src="https://www.amcharts.com/lib/4/charts.js" defer></script>
    <script src="https://www.amcharts.com/lib/4/themes/dark.js" defer></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js" defer></script>

    <!-- webticker -->
    <script src="/template/assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js" defer></script>

    <!-- Sparkline -->
    <script src="/template/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js" defer></script>

    <!-- Crypto_Admin App -->
    <script src="/template/html/js/template.js" defer></script>

    <!-- Crypto_Admin dashboard demo (This is only for demo purposes) -->
    <script src="/template/html/js/pages/dashboard.js" defer></script>
    <script src="/template/html/js/pages/dashboard-chart.js" defer></script>

    <!-- Crypto_Admin for demo purposes -->
    <script src="/template/html/js/demo.js" defer></script>

    <!-- This is data table -->
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js" defer></script>

    <!-- start - This is for export functionality only -->
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js"
        defer>
    </script>
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js" defer>
    </script>
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js" defer></script>
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js" defer></script>
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js" defer></script>
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js" defer>
    </script>
    <script src="/template/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js" defer>
    </script>
    <!-- end - This is for export functionality only -->

    <script>
        window.onload = function(){
            Array.from(document.querySelectorAll('.data-table')).forEach(el => {
                if(el.classList.contains('export-table')){
                    $(el).DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            // 'copy', 'csv', 'excel', 'pdf', 'print',
                            {
                                extend: 'excel',
                                title: 'Data export'
                            }
                        ]
                    });    
                }else{
                    $(el).DataTable();
                }
            });
        }
    </script>

    <script src="/template/assets/vendor_components/chart.js-master/Chart.min.js"></script>

    @yield('css')
    @yield('js')
</body>

</html>