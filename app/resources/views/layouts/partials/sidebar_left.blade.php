<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="ulogo">
                <a href="index.html">
                    <!-- logo for regular state and mobile devices -->
                    {{-- <span><b>New Digital </b>Time</span> --}}
                </a>
            </div>
            <div class="image text-center" style="height: 100px">
                <a href="/">
                    <img src="{{ asset('img/logo.png') }}" class="" alt="Logo" style="max-width: 75%; border: none;" />
                </a>
            </div>
        </div>
        <!-- sidebar menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="nav-devider"></li>
            @role('Admin')
            <li class="header nav-small-cap">Admin</li>
            <li
                class="treeview {{ in_array($_SERVER['REQUEST_URI'],["/admin/users","/admin/orders"]) ? 'active menu-open' : '' }}">
                <a href="#">
                    <i class="icon-chart"></i>
                    <span>General</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/users" ? 'active' : '' }}">
                        <a href="{{ route('admin.users.index') }}">Admin Usuarios</a>
                    </li>
                    <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/transactions" ? 'active' : '' }}">
                        <a href="{{ route('admin.transactions.index') }}">Aprobar transacciones</a>
                    </li>
                    <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/transactions" ? 'active' : '' }}">
                        <a href="{{ route('admin.settings.index') }}">Configuraciones</a>
                    </li>
                    <li class="treeview {{ $_SERVER['REQUEST_URI'] == "/admin/transactions" ? 'active' : '' }}">
                        <a href="#">
                            <span>Historial</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/transactions" ? 'active' : '' }}">
                                <a href="{{ route('admin.histories.supports') }}">Soportes</a>
                            </li>
                            <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/transactions" ? 'active' : '' }}">
                                <a href="{{ route('admin.histories.commissions') }}">Comisiones</a>
                            </li>
                            <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/orders" ? 'active' : '' }}">
                                <a href="{{ route('admin.ordersPackages.index') }}">Historial de compras</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ $_SERVER['REQUEST_URI'] == "/admin/transactions" ? 'active' : '' }}">
                        <a href="{{ route('admin.histories.users_lost') }}">Usuarios desactivados</a>
                    </li>
                </ul>
            </li>
            @endrole
            <li class="header nav-small-cap">PERSONAL</li>
            <li class="{{ in_array($_SERVER['REQUEST_URI'],['/','/home']) ? 'active' : '' }}">
                <a href="{{ route('home') }}">
                    <i class="icon-home"></i> <span>Dashboard</span>
                </a>
            </li>
            <li
                class="{{ $_SERVER['REQUEST_URI'] == "/users/" . Auth::user()->username . "/referrals" ? 'active' : '' }}">
                <a href="{{ route('users.referrals.index', Auth::user()->username) }}">
                    <i class="icon-people"></i> <span>Referidos</span>
                </a>
            </li>
            <li class="{{ $_SERVER['REQUEST_URI'] == "/users/tree" ? 'active' : '' }}">
                <a href="{{ route('users.tree') }}">
                    <i class="icon-people"></i> <span>Arbol</span>
                </a>
            </li>
            <li
                class="{{ $_SERVER['REQUEST_URI'] == "/users/" . Auth::user()->username . "/wallets" ? 'active' : '' }}">
                <a href="{{ route('users.wallets.index', Auth::user()->username) }}">
                    <i class="icon-people"></i> <span>Wallet</span>
                </a>
            </li>
            <li class="{{ $_SERVER['REQUEST_URI'] == "/commissions" ? 'active' : '' }}">
                <a href="{{ route('commissions.index') }}">
                    <i class="icon-people"></i> <span>Comisiones</span>
                </a>
            </li>
            <li class="{{ $_SERVER['REQUEST_URI'] == "/packages" ? 'active' : '' }}">
                <a href="{{ route('packages.index') }}">
                    <i class="icon-people"></i> <span>Paquetes</span>
                </a>
            </li>
            <li class="{{ $_SERVER['REQUEST_URI'] == "/orders" ? 'active' : '' }}">
                <a href="{{ route('ordersPackages.index') }}">
                    <i class="icon-people"></i> <span>Historial de compras</span>
                </a>
            </li>
            <li class="{{ $_SERVER['REQUEST_URI'] == "/supportTickets" ? 'active' : '' }}">
                <a href="{{ route('supportTickets.index') }}">
                    <i class="icon-people"></i> <span>Soporte</span>
                </a>
            </li>
        </ul>
    </section>
</aside>