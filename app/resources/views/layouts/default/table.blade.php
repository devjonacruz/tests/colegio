@if(isset($crear))
<div class="my-4 d-flex justify-content-end">
    <a href="{{ $crear->url }}" class="btn btn-primary">
        {{ $crear->text }}
    </a>
</div>
@endif
<div class="table-responsive">
    <table
        class="table text-center {{ isset($datatable) ? 'data-table' : '' }} {{ isset($export) ? 'export-table' : '' }}">
        <thead>
            <tr>
                <th>#</th>
                @foreach ($headers as $header)
                <th>{{ $header }}</th>
                @endforeach
                @if(isset($actions) && !empty($actions))
                @foreach ($actions[0] as $a)
                <th></th>
                @endforeach
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($body as $k => $row)
            <tr>
                <td>{{ $k + 1 }}</td>
                @foreach ($row as $v)
                <td>{{ $v }}</td>
                @endforeach
                @if(isset($actions) && !empty($actions))
                @foreach ($actions[$k] as $key => $action)
                <td>
                    @php
                    switch ($key) {
                    case 'edit':
                    @endphp
                    <a href="{{ $action['url'] }}" rel="noopener noreferrer">
                        <i class="fa fa-edit text-success"></i>
                    </a>
                    @php
                    break;

                    case 'check':

                    if($action['url'] == false) break;

                    @endphp
                    <form-component method="PUT" action="{{ $action['url'] }}" v-slot="slotProps">
                        <button type="button" @click="slotProps.onSubmit">
                            <i class="fa fa-check text-success"></i>
                        </button>
                    </form-component>
                    @php
                    break;

                    case 'close':

                    if($action['url'] == false) break;
                    @endphp
                    <form-component method="PUT" action="{{ $action['url'] }}" v-slot="slotProps">
                        <button type="button" @click="slotProps.onSubmit">
                            <i class="fa fa-close text-danger"></i>
                        </button>
                    </form-component>
                    @php
                    break;

                    default:
                    break;
                    }
                    @endphp
                </td>
                @endforeach
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>