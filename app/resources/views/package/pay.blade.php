<div class="row">
    <div class="col-12 text-center">
        <img src="{{ $qrcode_url }}" alt="">
        <div>{{ $address }}</div>
        <div>{{ $amount }}</div>
    </div>
    <div class="col-12 text-center">
        {{ $message }}
    </div>
</div>