@extends('layouts.authentication')

@section('content')
<div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>
    <p class="login-box-msg">Sponsor: {{ $parent->username }}</p>

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <input type="hidden" name="parent" id="parent" value="{{ $parent->username }}">

        <div class="form-group has-feedback">
            <input required id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror"
                placeholder="{{ __('Name') }}" value="{{ old('name') }}" />
            @if(!$errors->has('name'))
            <span class="ion ion-person form-control-feedback"></span>
            @endif
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
            <input required id="last_name" name="last_name" type="text"
                class="form-control @error('last_name') is-invalid @enderror" placeholder="{{ __('Last Name') }}"
                value="{{ old('last_name') }}" />
            @if(!$errors->has('last_name'))
            <span class="ion ion-person form-control-feedback"></span>
            @endif
            @error('last_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
            <input required id="username" name="username" type="text"
                class="form-control @error('username') is-invalid @enderror" placeholder="{{ __('Username') }}"
                value="{{ old('username') }}" />
            @if(!$errors->has('username'))
            <span class="ion ion-person form-control-feedback"></span>
            @endif
            @error('username')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
            <input required id="email" name="email" type="email"
                class="form-control @error('email') is-invalid @enderror" placeholder="{{ __('Email') }}"
                value="{{ old('email') }}" />
            @if(!$errors->has('email'))
            <span class="ion ion-email form-control-feedback"></span>
            @endif
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
            <input required id="password" name="password" type="password"
                class="form-control @error('password') is-invalid @enderror" placeholder="{{ __('Password') }}"
                value="{{ old('password') }}" />
            @if(!$errors->has('password'))
            <span class="ion ion-locked form-control-feedback"></span>
            @endif
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
            <input required id="password_confirmation" name="password_confirmation" type="password"
                class="form-control @error('password_confirmation') is-invalid @enderror"
                placeholder="{{ __('Retype password') }}" value="{{ old('password_confirmation') }}" />
            @if(!$errors->has('password_confirmation'))
            <span class="ion ion-log-in form-control-feedback"></span>
            @endif
            @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <div class="form-check">
                <input class="form-check-input @error('terms') is-invalid @enderror" type="checkbox" id="terms"
                    name="terms">
                <label class="form-check-label" for="terms">
                    <a href="#"><b>I agree to the Terms</b></a>
                </label>
                @error('terms')
                <div class="invalid-feedback">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-info btn-block margin-top-10">SIGN UP</button>
            </div>
        </div>
    </form>

    <div class="social-auth-links text-center d-none">
        <p>- OR -</p>
        <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
    </div>
    <!-- /.social-auth-links -->

    <div class="margin-top-20 text-center">
        <p>Already have an account?<a href="{{ route('login') }}" class="text-info m-l-5"> Sign In</a></p>
    </div>
</div>
@endsection