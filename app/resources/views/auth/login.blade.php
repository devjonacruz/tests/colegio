@extends('layouts.authentication')

@section('content')
<div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group has-feedback">
            <input required id="email" name="email" type="text"
                class="form-control @error('email') is-invalid @enderror" placeholder="{{ __('Username or Email') }}"
                value="{{ old('email') }}" />
            @if(!$errors->has('email'))
            <span class="ion ion-email form-control-feedback"></span>
            @endif
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>


        <div class="form-group has-feedback">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
            @if(!$errors->has('password'))
            <span class="ion ion-locked form-control-feedback"></span>
            @endif
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
            <input id="code_2fa" name="code_2fa" type="text"
                class="form-control @error('code_2fa') is-invalid @enderror" placeholder="{{ __('2FA (Optional)') }}"
                value="{{ old('code_2fa') }}" />
            @if(!$errors->has('code_2fa'))
            <span class="ion ion-locked form-control-feedback"></span>
            @endif
            @error('code_2fa')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-6">
                <div class="checkbox">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                    <label for="remember">{{ __('Remember Me') }}</label>
                </div>
            </div>

            @if (Route::has('password.request'))
            <div class="col-6">
                <div class="fog-pwd">
                    <a href="{{ route('password.request') }}"><i class="ion ion-locked"></i>
                        {{ __('Forgot Your Password?') }}</a><br />
                </div>
            </div>
            @endif

            <div class="col-12 text-center">
                <button type="submit" class="btn btn-info btn-block margin-top-10">
                    {{ __('SIGN IN')}}
                </button>
            </div>
        </div>
    </form>

    <div class="social-auth-links text-center d-none">
        <p>- OR -</p>
        <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
    </div>

    <div class="margin-top-30 text-center">
        <?php
        $userOne = \App\User::find(1);
        ?>
        <p>
            Don't have an account?
            <a href="{{ route('register') }}/{{ $userOne->username }}" class="text-info m-l-5">Sign Up</a>
        </p>
    </div>
</div>

@endsection