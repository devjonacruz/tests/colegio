"use strict";

import axios from "axios";

let api = {
    async onSubmit(e) {
        let formData = new FormData(e),
            method = formData.get("_method")
                ? formData.get("_method").toLowerCase()
                : "get",
            url = e.getAttribute("action"),
            param =
                method == "post" || method == "put" ? objForm(formData) : {};

        await axios[method](url, param);
    }
};

function objForm(formData) {
    formData.delete("_method");
    let obj = {};
    for (const iterator of formData.entries()) {
        obj[iterator[0]] = iterator[1];
    }
    return obj;
}

export default api;
