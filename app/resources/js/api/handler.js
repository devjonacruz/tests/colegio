import axios from "axios";
import toast from "./toast";

function successRequestHandler(config) {
    config.headers["Content-Type"] = "application/json";
    return config;
}

function errorRequestHandler(err) {
    return Promise.reject(err);
}

function successResponseHandler(res) {
    if (typeof res === "object") {
        if (res.data.message) {
            toast[res.data.status || "success"](res.data.message);
        }
        if (res.data.html) {
            const { html } = res.data;
            if (html.type == "modal") {

                let element = html.element || "#modalGenericDefault"
                
                $(element).modal("show");

                if (html.header) {
                    $(element)
                        .find(".modal-title")
                        .html(html.header);
                } else {
                    $(element)
                        .find(".modal-header")
                        .remove();
                }

                if (html.body) {
                    $(element)
                        .find(".modal-body")
                        .html(html.body);
                } else {
                    $(element)
                        .find(".modal-body")
                        .remove();
                }

                if (html.footer) {
                    $(element)
                        .find(".modal-footer")
                        .html(html.footer);
                } else {
                    $(element)
                        .find(".modal-footer")
                        .remove();
                }

                if (html.content) {
                    $(element)
                        .find(".modal-content")
                        .html(html.content);
                }
            }
        }
    }

    return res;
}

function errorResponseHandler(err) {
    let { message, errors } = err.response.data;

    if (message) {
        toast.error(message);
    }

    if (err.response.status === 400) {
        if (errors) {
            // for (const key in errors) {
            //     if (Object.hasOwnProperty.call(errors, key)) {
            //         const element = errors[key];
            //         console.log(element);
            //     }
            // }

            for (const iterator of errors) {
                toast.error(iterator);
            }
        }
    }
}

axios.interceptors.request.use(successRequestHandler, errorRequestHandler);
axios.interceptors.response.use(successResponseHandler, errorResponseHandler);

export default {};
