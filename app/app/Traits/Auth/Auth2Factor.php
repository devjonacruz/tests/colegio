<?php


namespace App\Traits\Auth;

use App\User;
use RobThree\Auth\TwoFactorAuth;

trait Auth2Factor
{
    protected function isValid(String $code, User $user)
    {
        $tfa = new TwoFactorAuth(env('APP_NAME'));

        if (!env('CODE_2FA', true)) {
            return true;
        }

        if (!$user->authentication) {
            return false;
        }

        if (!$user->authentication->is_google) {
            return false;
        }

        if ($tfa->verifyCode($user->authentication->google_secret, $code) === true) {
            return true;
        }

        return false;
    }
}
