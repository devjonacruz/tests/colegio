<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class SettingsType extends Model
{
    protected $fillable = ['name', 'alias'];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->alias = Str::slug($model->name, '-');
        });
    }

    public function getRouteKeyName()
    {
        return 'alias';
    }

    /**
     * Get all of the settings for the SettingsType
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settings(): HasMany
    {
        return $this->hasMany(Setting::class);
    }
}
