<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeWallet extends Model
{
    protected $fillable = ['name', 'is_available'];
}
