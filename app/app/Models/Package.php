<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class Package extends Model
{
    protected $fillable = ['name', 'alias', 'image', 'amount'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'decimal:2',
        'is_visible' => 'boolean',
    ];
    
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->alias = Str::slug($model->name, '-');
        });
    }

    public function getRouteKeyName()
    {
        return 'alias';
    }

    /**
     * Get all of the orders_packages_commission for the Package
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders_packages_commission(): HasMany
    {
        return $this->hasMany(OrdersPackagesCommission::class);
    }
}
