<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBroker extends Model
{
    protected $fillable = ['user_id', 'broker_id', 'broker_server', 'broker_pass'];
}
