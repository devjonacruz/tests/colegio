<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Commission extends Model
{
    protected $fillable = ['user_id', 'user_grant_id', 'amount'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'decimal:2',
    ];
    
    /**
     * Get the user that owns the Commission
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the user_grant that owns the Commission
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_grant(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_grant_id');
    }
}
