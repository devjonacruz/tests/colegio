<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class Setting extends Model
{
    protected $fillable = ['settings_type_id', 'log', 'alias'];

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->alias = Str::slug($model->settins_type->name, '-');
        });
    }

    public function getRouteKeyName()
    {
        return 'alias';
    }
    
    /**
     * Get the settins_type that owns the Setting
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function settins_type(): BelongsTo
    {
        return $this->belongsTo(SettingsType::class, 'settings_type_id');
    }
}
