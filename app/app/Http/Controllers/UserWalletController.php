<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\Payments\Coinpayments;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\Auth\Auth2Factor;
use Illuminate\Support\Str;

class UserWalletController extends Controller
{
    use Auth2Factor;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $headers = [
            'Tipo',
            'Estado',
            'Valor',
            'Fecha',
        ];

        $body = $user
            ->transactions
            ->map(function ($transaction) {
                return [
                    $transaction->type_transaction->name,
                    $transaction->state->name,
                    $transaction->amount,
                    $transaction->created_at->diffForHumans(),
                ];
            });

        return view('user.wallet', compact('user', 'headers', 'body'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $rules = [
            'type' => 'required',
            'amount' => 'required',
            'code_2fa' => 'required|digits:6',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }

        $setting = Setting::where('settings_type_id', 1)->first();

        if (!(bool)$setting->log) {
            return response()->json([
                'status' => 'error',
                'message' => "Los retiros no están habilitados, por favor validar con administración.",
            ], 401);
        }

        if (!$this->isValid($request->code_2fa, $user)) {
            return response()->json([
                'status' => 'error',
                'message' => "El código no es correcto",
            ]);
        }

        $wallet = Wallet::where('type_wallet_id', $request->type)->where('user_id', $user->id)->first();

        if ($wallet->amount < $request->amount) {
            return response()->json([
                'status' => 'error',
                'message' => "No cuenta con saldo para retirar",
            ]);
        }

        if (empty($wallet->wallet)) {
            return response()->json([
                'status' => 'error',
                'message' => "Debe configurar una wallet",
            ]);
        }

        if (!$user->is_active) {
            return response()->json([
                'status' => 'error',
                'message' => "No puedes realizar retiros, por favor revisa con el adminisrador.",
            ], 401);
        }

        $invoice = Str::uuid();

        if (env('APP_ENV') == 'production') {
            $client = new Coinpayments();
            $client->createWithdrawal($request->amount, 'USDT', 'USD', $wallet->wallet, $invoice);
            $log = ['response' => $client->response, 'wallet' => $wallet->wallet];
        } else {
            $log = ['wallet' => $wallet->wallet];
        }

        Transaction::create([
            'user_id' => $user->id,
            'type_transaction_id' => 1,
            'type_wallet_id' => $wallet->type_wallet_id,
            'amount' => $request->amount,
            'invoice' => $invoice,
            'log' => json_encode($log),
        ]);

        $wallet->amount -= $request->amount;
        $wallet->save();

        return response()->json([
            'status' => 'success',
            'message' => "Transacción creada",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'type' => 'required|exists:type_wallets,id',
            'wallet' => 'required',
            'code_2fa' => 'required|digits:6',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }


        if (!$this->isValid($request->code_2fa, $user)) {
            return response()->json([
                'status' => 'error',
                'message' => "El código no es correcto",
                'data' => null
            ]);
        }

        Wallet::updateOrCreate(
            ['user_id' => $user->id, 'type_wallet_id' => $request->type],
            [
                'wallet' => $request->wallet,
            ]
        );

        return response()->json([
            'status' => 'success',
            'message' => "Información actualizada",
            'data' => null
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
