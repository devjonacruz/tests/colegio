<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\UserAuthentication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RobThree\Auth\TwoFactorAuth;

class UserAuthenticationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'code_2fa' => 'required|digits:6',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }

        $tfa = new TwoFactorAuth(env('APP_NAME'));
        $verifyCode = $tfa->verifyCode($user->authentication->google_secret, $request->code_2fa);

        if ($verifyCode === false) {
            return response()->json([
                'status' => 'error',
                'message' => "El código no es correcto",
                'data' => null
            ]);
        }

        UserAuthentication::where('user_id', $user->id)->update([
            'user_id' => $user->id,
            'is_google' => !$user->authentication->is_google,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => "Información actualizada",
            'data' => null
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
