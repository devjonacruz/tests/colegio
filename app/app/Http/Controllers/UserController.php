<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResources;
use App\Http\Resources\UserCollection;
use App\Models\UserAuthentication;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use RobThree\Auth\TwoFactorAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $tfa = new TwoFactorAuth(env('APP_NAME'));

        if (!$user->authentication) {

            UserAuthentication::create([
                'user_id' => $user->id,
                'is_google' => false,
                'google_secret' => $tfa->createSecret(),
            ]);

            $user->refresh();
        }

        if (!$user->authentication->is_google) {
            $user->authentication->google_secret = $tfa->createSecret();
            $user->authentication->save();
        }

        $qr = $tfa->getQRCodeImageAsDataUri("$user->name $user->last_name", $user->authentication->google_secret);

        return view('user.edit', compact('user', 'qr'));
    }

    /**
     * Update the specified resource in storage.
     * @param \App\Http\Requests\User $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'password' => 'confirmed',
            'phone' => '',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }

        collect($request->only(array_keys($rules)))->each(function ($value, $key) use (&$user) {
            if (is_null($value)) return;

            switch ($key) {
                case 'password':
                    $user->{$key} = Hash::make($value);
                    break;

                default:
                    $user->{$key} = $value;
                    break;
            }
        });

        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Se actualiza la información',
            'data' => new UserResources($user)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function tree()
    {
        return view('user.tree');
    }
}
