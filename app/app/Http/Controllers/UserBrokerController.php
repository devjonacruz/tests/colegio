<?php

namespace App\Http\Controllers;

use App\Models\UserBroker;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\Auth\Auth2Factor;

class UserBrokerController extends Controller
{
    use Auth2Factor;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'broker_id' => 'required',
            'broker_server' => 'required',
            'broker_pass' => 'required',
            'code_2fa' => 'required|digits:6',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }


        if (!$this->isValid($request->code_2fa, $user)) {
            return response()->json([
                'status' => 'error',
                'message' => "El código no es correcto",
                'data' => null
            ]);
        }

        UserBroker::updateOrCreate(
            ['user_id' => $user->id],
            [
                'broker_id' => $request->broker_id,
                'broker_server' => $request->broker_server,
                'broker_pass' => $request->broker_pass,
            ]
        );

        return response()->json([
            'status' => 'success',
            'message' => "Información actualizada",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
