<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest';
        // $url = 'https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/listings/latest';
        // $parameters = [
        //     'start' => '1',
        //     'limit' => '5000',
        //     'convert' => 'USD'
        // ];

        // $headers = [
        //     'Accepts: application/json',
        //     'X-CMC_PRO_API_KEY: b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c',
        // ];
        // $qs = http_build_query($parameters); // query string encode the parameters
        // $request = "{$url}?{$qs}"; // create the request URL


        // $curl = curl_init(); // Get cURL resource
        // // Set cURL options
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => $request,            // set the request URL
        //     CURLOPT_HTTPHEADER => $headers,     // set the headers 
        //     CURLOPT_RETURNTRANSFER => 1         // ask for raw response instead of bool
        // ));

        // $response = curl_exec($curl); // Send the request, save the response
        // // print_r(json_decode($response)); // print json decoded response
        // curl_close($curl);

        // $res = json_decode($response);
        // dd($res->data[0]);

        $diffInDays = 0;
        $diffInDaysActive = 0;
        $diffInDaysPending = 1;
        if (Auth::user()->active_start) {
            $to = Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->active_start);
            $from = Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->active_end);
            $current = Carbon::createFromFormat('Y-m-d H:i:s', now());

            $diffInDays = $to->diffInDays($from);
            $diffInDaysPending = $current->diffInDays($from);
            $diffInDaysActive = $diffInDays - $diffInDaysPending;
            // dd($diffInDays, $diffInDaysActive, $diffInDaysPending);
        }
        return view('home', compact('diffInDaysActive', 'diffInDaysPending'));
    }
}
