<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Commission;
use App\Models\SupportTicket;
use App\User;

class HistoryController extends Controller
{
    public function supports()
    {
        $title = 'Historial de soportes';

        $headers = [
            'Username',
            'Asunto',
            'Estado',
            'Fecha',
            'Action',
        ];

        $actions = [];

        $body = SupportTicket::orderBy('created_at', 'DESC')
            ->get()->map(function ($item) use (&$actions) {
                $actions[] = [
                    'edit' => [
                        'url' => route('supportTickets.show', $item->id),
                    ],
                ];
                return [
                    $item->user->username,
                    $item->subject,
                    $item->state->name,
                    $item->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;

        $tickets = SupportTicket::orderBy('created_at', 'DESC')->get();

        $total = $tickets->count();
        $responded = $tickets->filter(function ($ticket) {
            return $ticket->states_ticket_id == 3;
        })->count();
        $resolve = $tickets->filter(function ($ticket) {
            return $ticket->states_ticket_id == 2;
        })->count();
        $pending = $tickets->filter(function ($ticket) {
            return $ticket->states_ticket_id == 1;
        })->count();

        return view('tickets.index', compact('tickets', 'total', 'responded', 'resolve', 'pending', 'datatable', 'export'));
    }

    public function commissions()
    {
        $title = 'Historial de comisiones';

        $headers = [
            'Usuario que recibe',
            'Usuario',
            'Monto',
            'Fecha',
        ];

        $actions = [];

        $body = Commission::orderBy('created_at', 'DESC')
            ->get()->map(function ($item) use (&$actions) {
                return [
                    $item->user->username,
                    $item->user_grant->username,
                    $item->amount,
                    $item->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;

        return view('table', compact('headers', 'body', 'actions', 'title', 'datatable', 'export'));
    }

    public function users()
    {
        $title = 'Usuarios desactivados';

        $headers = [
            'Usuario',
            'Email',
            'Fecha de registro',
        ];

        $actions = [];

        $body = User::where('is_active', false)->orderBy('created_at', 'DESC')
            ->get()->map(function ($item) use (&$actions) {
                return [
                    $item->username,
                    $item->email,
                    $item->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;

        return view('table', compact('headers', 'body', 'actions', 'title', 'datatable', 'export'));
    }
}
