<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\SettingsType;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Lista de configuraciones';

        $headers = [
            'Nombre',
            'Valor',
            'Fecha',
        ];

        $actions = [];

        $body = Setting::orderBy('created_at', 'DESC')
            ->get()->map(function ($item) use (&$actions) {
                $valor = '';
                switch ($item->settings_type_id) {
                    case 1:
                        $valor = $item->log ? 'SI' : 'NO';
                        $actions[] = [
                            $item->log ? "close" : 'check' => [
                                'url' =>  route('api.admin.settings.update', $item->settins_type->alias),
                            ],
                        ];
                        break;

                    case 2:
                        $valor = $item->log;
                        $actions[] = [
                            'edit' => [
                                'url' =>  route('admin.settings.edit', $item->settins_type->alias),
                            ],
                        ];
                        break;

                    default:
                        # code...
                        break;
                }

                return [
                    $item->settins_type->name,
                    $valor,
                    $item->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;
        $crear = (object)[
            'text' => 'Crear Configuración',
            'url' => route('admin.settings.create'),
        ];

        return view('table', compact('headers', 'body', 'actions', 'title', 'datatable', 'export', 'crear'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = SettingsType::orderBy('name')->get();
        return view('admin.settings.setting.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('admin.settings.setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        switch ($setting->settings_type_id) {
            case 1:
                $setting->log = !(bool)$setting->log;
                $setting->save();

                return response()->json([
                    'message' => 'Se actualiza la información',
                ]);
                break;

            case 2:
                $setting->log = $request->value;
                $setting->save();

                return response()->json([
                    'message' => 'Se actualiza la información',
                ]);
                break;

            default:
                return response()->json([
                    'status' => 'success',
                    'message' => 'Información no encontrada',
                ]);
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
