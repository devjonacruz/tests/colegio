<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\User as UserResources;
use App\Models\UserAuthentication;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Lista de usuarios';

        $headers = [
            'Username',
            'Nombre',
            'Apellido',
            'Email',
            'Registro',
        ];

        $actions = [];

        $body = User::orderBy('created_at', 'DESC')
            ->get()->map(function ($user) use (&$actions) {
                $actions[] = [
                    'edit' => [
                        'url' => route('admin.users.edit', $user->username),
                    ],
                ];
                return [
                    $user->username,
                    $user->name,
                    $user->last_name,
                    $user->email,
                    $user->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;

        return view('table', compact('headers', 'body', 'actions', 'title', 'datatable', 'export'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.settings.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required',
        ])->validate();

        $user = User::create($request->all());
        return response()->json([
            'message' => 'Se actualiza la información',
            'data' => new UserResources($user)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.settings.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'last_name' => 'required',
            'password' => 'confirmed',
            'phone' => '',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => "",
                'errors' => $validator->errors()->all(),
            ], 400);
        }

        collect($request->only(array_keys($rules)))->each(function ($value, $key) use (&$user) {
            if (is_null($value)) return;

            switch ($key) {
                case 'password':
                    $user->{$key} = Hash::make($value);
                    break;
                default:
                    $user->{$key} = $value;
                    break;
            }
        });

        $user->save();

        UserAuthentication::updateOrCreate(
            ['user_id' => $user->id],
            ["is_google" => $request->has('code_2fa') ? true : false]
        );

        return response()->json([
            'status' => 'success',
            'message' => 'Se actualiza la información',
            'data' => null
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
            'message' => 'Se actualiza la información',
            'data' => ''
        ]);
    }
}
