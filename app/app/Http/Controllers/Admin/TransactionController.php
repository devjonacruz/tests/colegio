<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessTransaction;
use App\Models\Transaction;
use Exception;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Lista de retiros';

        $headers = [
            'Usuario',
            'Valor',
            'Estado',
            'Wallet',
            'Fecha',
        ];

        $actions = [];

        $body = Transaction::orderBy('state_id')
            ->orderBy('created_at', 'DESC')
            ->get()->map(function ($item) use (&$actions) {
                $actions[] = [
                    'check' => [
                        'url' => in_array($item->state_id, [1, 2]) ? route('api.transactions.approved', $item->id) : false,
                    ],
                    'close' => [
                        'url' => in_array($item->state_id, [1, 2]) ? route('api.transactions.cancelled', $item->id) : false,
                    ],
                ];

                $log = json_decode($item->log);

                return [
                    $item->user->username,
                    $item->amount,
                    $item->state->name,
                    $log->wallet,
                    $item->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;

        return view('table', compact('headers', 'body', 'actions', 'title', 'datatable', 'export'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function approved(Transaction $transaction)
    {
        $this->validator($transaction);

        $transaction->state_id = 6;
        $transaction->save();

        return response()->json([
            'message' => 'Se actualiza la información',
        ]);
    }

    public function cancelled(Transaction $transaction)
    {
        $this->validator($transaction);

        $transaction->state_id = 5;
        $transaction->save();

        ProcessTransaction::dispatch($transaction)->delay(now())->onQueue('Transaction');

        return response()->json([
            'message' => 'Se actualiza la información',
        ]);
    }

    public function validator(Transaction $transaction)
    {
        if (!in_array($transaction->state_id, [1, 2])) {
            throw new Exception("No puede modificar una transacción ya aprobada o cancelada", 1);
        }
    }
}
