<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessOrdersPackage;
use App\Models\OrdersPackage;
use App\Models\Package;
use App\Services\Payments\Coinpayments;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Lista de compras';

        $headers = [
            'Usuario',
            'Paquete',
            'Valor',
            'Estado',
            'Fecha',
        ];

        $actions = [];

        $body = OrdersPackage::orderBy('created_at', 'DESC')
            ->get()->map(function ($order) use (&$actions) {
                $actions[] = [
                    'check' => [
                        'url' => in_array($order->state_id, [1, 2]) ? route('api.orderPackage.approved', $order->id) : false,
                    ],
                    'close' => [
                        'url' => in_array($order->state_id, [1, 2]) ? route('api.orderPackage.cancelled', $order->id) : false,
                    ],
                ];

                return [
                    $order->user->username,
                    $order->package->name,
                    $order->package->amount,
                    $order->state->name,
                    $order->created_at->diffForHumans(),
                ];
            });

        $datatable = true;
        $export = true;

        return view('table', compact('headers', 'body', 'actions', 'title', 'datatable', 'export'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function show(OrdersPackage $ordersPackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdersPackage $ordersPackage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdersPackage $ordersPackage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrdersPackage  $ordersPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrdersPackage $ordersPackage)
    {
        //
    }

    public function approved(OrdersPackage $ordersPackage)
    {
        $this->validator($ordersPackage);

        $ordersPackage->state_id = 3;
        $ordersPackage->save();

        ProcessOrdersPackage::dispatch($ordersPackage)->delay(now())->onQueue('OrdersPackage');

        return response()->json([
            'message' => 'Se actualiza la información',
        ]);
    }

    public function cancelled(OrdersPackage $ordersPackage)
    {
        $this->validator($ordersPackage);

        $ordersPackage->state_id = 5;
        $ordersPackage->save();

        return response()->json([
            'message' => 'Se actualiza la información',
        ]);
    }

    public function validator(OrdersPackage $ordersPackage)
    {
        if (!in_array($ordersPackage->state_id, [1, 2])) {
            throw new Exception("No puede modificar una compra ya aprobada o cancelada", 1);
        }
    }
}
