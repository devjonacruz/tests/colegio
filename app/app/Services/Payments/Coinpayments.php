<?php

namespace App\Services\Payments;

use App\Jobs\ProcessOrdersPackage;
use App\Models\OrdersPackage;
use CoinpaymentsAPI;
use Exception;
use Illuminate\Http\Request;

class Coinpayments
{
    private static $instance;

    public $response;

    public function __construct()
    {
        self::$instance = new CoinpaymentsAPI(env('COINPAYMENTS_PRIVATE_KEY'), env('COINPAYMENTS_PUBLIC_KEY'), 'json');

        return self::$instance;
    }

    /**
     * createCustomTransaction
     * @param [type] $amount
     * @param [type] $currency1
     * @param [type] $currency2
     * @param [type] $buyer_email
     * @return mixed
     */
    public function createCustomTransaction($amount, $currency1, $currency2, $buyer_email)
    {
        $ipn_url = env('COINPAYMENTS_URL_CONFIRMATION');
        $response = self::$instance->CreateCustomTransaction(compact('amount', 'currency1', 'currency2', 'buyer_email', 'ipn_url'));

        $this->response = $this->responseRequest($response);
    }

    /**
     * createWithdrawal
     * @param [type] $amount
     * @param [type] $currency
     * @param [type] $address
     * @param [type] $ipn_url
     * @param integer $auto_confirm
     * @return mixed
     */
    public function createWithdrawal($amount, $currency, $currency2 = 'USD', $address, $invoice, $auto_confirm = 0)
    {
        $ipn_url = env('COINPAYMENTS_URL_WITHDRAWAL') . '/' . $invoice;
        $response = self::$instance->CreateWithdrawal(compact('amount', 'currency', 'currency2', 'address', 'ipn_url', 'auto_confirm'));

        $this->response = $this->responseRequest($response);
    }

    protected function responseRequest($response)
    {
        if (isset($response['error']) && $response['error'] == 'ok') {
            return is_array($response['result']) ? (object)$response['result'] : $response['result'];
        }

        throw new Exception($response['error'], 1);
    }

    public function confirmation(Request $request)
    {
        $txn_id = $request->txn_id;
        $status = intval($request->status);

        $order = OrdersPackage::where('invoice', $txn_id)->first();

        if (is_null($order)) {
            return;
        }

        // $this->util->insert([
        //     'table' => 'aud_transaction',
        //     'fields' => [
        //         'id_transaction' => $obj_transaction->id,
        //         'request' => json_encode($this->post()),
        //     ]
        // ]);

        if ($status >= 100) {
            $this->accepted($order);
        } else if ($status < 0) {
            $this->rejected($order);
        }

        // (new ProcessOrdersPackage($order))->handle();
        ProcessOrdersPackage::dispatch($order)->delay(now())->onQueue('OrdersPackage');
    }

    protected function accepted($order)
    {
        $order->state_id = 3;
        $order->save();
    }
    protected function rejected($order)
    {
        if (in_array($order->state_id, [1, 2])) {
            $order->state_id = 4;
            $order->save();
        }
    }
}
